import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:testnemec/data/character.dart';
import 'package:testnemec/flow/character/repository/character_repository.dart';

part 'character_event.dart';
part 'character_state.dart';

@injectable
class CharacterBloc extends Bloc<CharacterEvent, CharacterState> {
  final CharacterRepository characterRepository;
  int page = 1;

  List<Character> overAllChars = [];

  CharacterBloc(this.characterRepository) : super(CharacterInitial()) {
    on<CharacterEvent>((event, emit) {});

    on<CharacterList>((event, emit) {
      event.scrollController.addListener(() {
        if (event.scrollController.position.atEdge) {
          if (event.scrollController.position.pixels != 0) {
            add(CharacterFetch());
          }
        }
      });
    });

    on<CharacterFetch>((event, emit) async {
      final characters = await characterRepository.fetchCharacters(page: page);

      if (characters != null) {
        overAllChars.addAll(characters);
        emit(CharacterLoaded(overAllChars));
      }

      page++;
    });
  }
}
