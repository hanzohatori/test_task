part of 'character_bloc.dart';

@immutable
abstract class CharacterEvent {}

class CharacterFetch extends CharacterEvent {}

class CharacterList extends CharacterEvent {
  final ScrollController scrollController;

  CharacterList(this.scrollController);
}
