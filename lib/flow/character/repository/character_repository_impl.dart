import 'dart:convert';

import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:testnemec/common/app_strings.dart';
import 'package:testnemec/data/character.dart';
import 'package:testnemec/flow/character/repository/character_repository.dart';

@Injectable(as: CharacterRepository)
class CharacterRepositoryImpl implements CharacterRepository {
  @override
  Future<List<Character>>? fetchCharacters({page}) async {
    final response = await get(
        Uri.parse(
            '${AppString.apiUrl}/character${page != null ? '?page=$page' : ''}'),
        headers: {'Accept': 'application/json'});
    return jsonDecode(response.body)['results']
        .map((e) => Character.fromJson(e))
        .toList()
        .cast<Character>();
  }
}
