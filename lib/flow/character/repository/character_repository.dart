import 'package:testnemec/data/character.dart';

abstract class CharacterRepository {
  Future<List<Character>>? fetchCharacters({page}) {}
}
