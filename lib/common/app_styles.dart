import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testnemec/common/app_colors.dart';

class AppTextStyles {
  static TextStyle h1 = GoogleFonts.lato(
    fontSize: 15,
    color: AppColors.black,
    fontWeight: FontWeight.w900,
  );
}
