class AppString {
  static const name = 'name';
  static const gender = 'gender';
  static const apiUrl = 'https://rickandmortyapi.com/api';
  static const status = 'status';
}
