import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:testnemec/flow/character/bloc/character_bloc.dart';
import 'package:testnemec/injection.dart';
import 'package:testnemec/router.dart';

void main() {
  configureDependencies();
  runApp(MyApp(router: AppRouter()));
}

class MyApp extends StatelessWidget {
  final AppRouter router;
  const MyApp({Key? key, required this.router}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt.get<CharacterBloc>(),
      child: Sizer(builder: (context, orientation, deviceType) {
        return MaterialApp(
          title: 'Test_nemec',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          debugShowCheckedModeBanner: false,
          onGenerateRoute: router.generateRouter,
        );
      }),
    );
  }
}
