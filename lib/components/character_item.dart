import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:testnemec/common/app_colors.dart';
import 'package:testnemec/common/app_strings.dart';
import 'package:testnemec/common/app_styles.dart';
import 'package:testnemec/data/character.dart';

class CharacterItem extends StatelessWidget {
  final Character character;
  const CharacterItem({
    Key? key,
    required this.character,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          Navigator.pushNamed((context), '/description', arguments: character),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: 120,
          height: 80,
          margin: EdgeInsets.all(15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            border: Border.all(color: AppColors.red),
          ),
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 20.w,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    image: DecorationImage(
                        image: NetworkImage(character.avatar ??
                            'https://timeweb.com/ru/community/article/67/67d62d1e0bc27de113cc0e25239705e2.png'),
                        fit: BoxFit.cover)),
              ),
              // ClipRRect(
              //   borderRadius: BorderRadius.circular(15),
              //   child: SvgPicture.network(
              //     'http://upload.wikimedia.org/wikipedia/commons/0/02/SVG_logo.svg',
              //     width: 30.w.sp,
              //     height: 10.h.sp,
              //     fit: BoxFit.fill,
              //   ),
              // ),
              const SizedBox(
                width: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppString.name + ":" '${character.name}',
                    style: AppTextStyles.h1,
                  ),
                  Row(
                    children: [
                      Text(
                        AppString.gender + ":" '${character.gender}',
                        style: AppTextStyles.h1,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: SvgPicture.network(
                          character.gender != 'Male'
                              ? 'https://upload.wikimedia.org/wikipedia/commons/b/bc/Female_symbol.svg'
                              : 'https://www.svgrepo.com/show/322751/male.svg',
                          width: 15.sp,
                          height: 15.sp,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
