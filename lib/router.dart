// import 'dart:js';

import 'package:flutter/material.dart';
import 'package:testnemec/data/character.dart';
import 'package:testnemec/screen/description_page.dart';
import 'package:testnemec/screen/home_page.dart';

class AppRouter {
  Route generateRouter(RouteSettings settings) {
    final arguments = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => HomePage());
      case '/description':
        return MaterialPageRoute(
            builder: (context) => DescriptionPage(
                  character: arguments as Character,
                ));

      default:
        return MaterialPageRoute(builder: (context) => HomePage());
    }
  }
}
