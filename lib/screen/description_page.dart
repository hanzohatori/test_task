import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:testnemec/common/app_strings.dart';
import 'package:testnemec/common/app_styles.dart';
import 'package:testnemec/data/character.dart';

class DescriptionPage extends StatefulWidget {
  final Character character;
  const DescriptionPage({
    Key? key,
    required this.character,
  }) : super(key: key);

  @override
  _DescriptionPageState createState() => _DescriptionPageState();
}

class _DescriptionPageState extends State<DescriptionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Desciptions'),
      ),
      body: SingleChildScrollView(
        child: Center(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              width: 25.w,
              height: 10.h,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(widget.character.avatar ??
                          'https://timeweb.com/ru/community/article/67/67d62d1e0bc27de113cc0e25239705e2.png'))),
            ),
            Text(
              widget.character.name,
              style: AppTextStyles.h1,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  AppString.gender + ":" '${widget.character.gender}',
                  style: AppTextStyles.h1,
                ),
                const SizedBox(
                  width: 10,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: SvgPicture.network(
                    widget.character.gender != 'Male'
                        ? 'https://upload.wikimedia.org/wikipedia/commons/b/bc/Female_symbol.svg'
                        : 'https://www.svgrepo.com/show/322751/male.svg',
                    width: 15.sp,
                    height: 15.sp,
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
            Text(
              '${AppString.status} : ${widget.character.status} ',
              style: AppTextStyles.h1,
            ),
            if (widget.character.episodes != null)
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Episodes',
                      style: AppTextStyles.h1,
                    ),
                    SizedBox(
                      height: 10.sp,
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) =>
                          Text(widget.character.episodes![index]),
                      itemCount: widget.character.episodes!.length,
                    ),
                  ],
                ),
              )
          ],
        )),
      ),
    );
  }
}
