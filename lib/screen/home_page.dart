import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sizer/sizer.dart';
import 'package:testnemec/common/app_colors.dart';
import 'package:testnemec/common/app_strings.dart';
import 'package:testnemec/common/app_styles.dart';
import 'package:testnemec/components/character_item.dart';
import 'package:testnemec/flow/character/bloc/character_bloc.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

late final String name;

class _HomePageState extends State<HomePage> {
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    context.read<CharacterBloc>().add(CharacterFetch());
    context.read<CharacterBloc>().add(CharacterList(scrollController));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocBuilder<CharacterBloc, CharacterState>(
          builder: (context, state) {
            if (state is! CharacterLoaded)
              return Center(
                child: CircularProgressIndicator(),
              );
            return ListView.builder(
                controller: scrollController,
                itemCount: state.characters.length,
                itemBuilder: (context, index) {
                  return CharacterItem(
                    character: state.characters[index],
                  );
                });
          },
        ),
      ),
    );
  }
}
