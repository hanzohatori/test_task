class Character {
  String? avatar;
  String name;
  String gender;
  List<String>? episodes;
  String status;

  Character.fromJson(Map json)
      : avatar = json['image'],
        name = json['name'],
        gender = json['gender'],
        episodes = json['episode'].cast<String>(),
        status = json['status'];
}
